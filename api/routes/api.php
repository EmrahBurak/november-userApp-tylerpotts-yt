<?php

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/users/create', function (Request $request) {
    $data = $request->all();

    if (!User::where('email', '=', $data['email'])->exists()) {
        $user = User::create([
            "name" => $data["name"],
            "email" => $data["email"],
            "password" => Hash::make($data["password"])
        ]);
        if (empty($user->id)) {
            return [
                "success" => false,
                "response" => [
                    "error" => "Un expected error has occured"
                ]
            ];
        } else {
            return [
                "success" => true,
		"response" => [
		   "user" => $user
		]
            ];
        }
    } else {
        return [
            "success" => false,
            "response" => [
                "error" => "The user allready exists"
            ]
        ];
    }
});

Route::get('/users/all', function (Request $request) {
    $users  = User::all();

    if (empty($users)) {
        return [
            "success" => false,
            "response" => [
                "error" => "No users found"
            ]
        ];
    } else {
        return [
            "success" => true,
            "response" => [
                "users" => $users
            ]
        ];
    }
});

Route::get('/users/{id}', function (Request $request, $id) {
    $user = User::find($id);

    if (empty($user)) {
        return [
            "success" => false,
            "response" => [
                "error" => "No user found"
            ]
        ];
    } else {
        return [
            "success" => true,
            "response" => [
                "user" => $user
            ]
        ];
    }
});

Route::delete('/users/delete/{id}',function(Request $request,$id){
    $user = User::find($id);


    if(empty($user)){
            $success = false;
            $response = [
                "error" => "User could not be deleted"
            ];
    }else{
            $success= $user->delete();
            $response =[
                "message"=> "User deleted"
            ];
    }

    return ["success"=>$success, "response"=>$response];
});


Route::put('/users/update/{id}',function(Request $request, $id){
    $data = $request->all();

    $user = User::find($id);

    foreach ($data as $key => $value) {
        $user->{$key} = $value;
    };


    $result = $user->save();

    return ["success"=>$result, "response"=>["user" => $user]];

});
